#ifndef TESTS_H
#define TESTS_H


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void common_malloc_test();

void free_one_block_test();

void free_two_blocks_test();

void memory_ended_case1_test();

void memory_ended_case2_test();
#endif