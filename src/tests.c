#include "tests.h"

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

/**
 * Тест на обычное успешное выделение памяти
 */
void common_malloc_test(){
    printf("\nTest1. Common malloc.\n");

    void* heap = heap_init(10000);

    printf("initial heap\n");
    debug_heap(stdout, heap);

    void* data = _malloc(100);
    printf("heap after allocation\n");
    debug_heap(stdout, heap);

    if (data != NULL && heap != NULL) printf("test 1 PASSED\n");
    if (data == NULL || heap == NULL) err("test 1 FAILED\n");
}

/**
 * Освобождение двух блоков из нескольких выделенных.
 */
void free_one_block_test(){
    printf("\nTest2. Free one block.\n");

    void* heap = heap_init(10000);

    void* data1 = _malloc(100);
    _malloc(150);

    printf("initial heap\n");
    debug_heap(stdout, heap);

    printf("heap after free\n");
    _free(data1);
    debug_heap(stdout, heap);

}

/**
 * Освобождение двух блоков из нескольких выделенных
 */
void free_two_blocks_test() {
    printf("\nTest3. Free two blocks.\n");

    void *heap = heap_init(10000);
    void *data1 = _malloc(200);
    void *data2 = _malloc(355);

    printf("initial heap\n");
    debug_heap(stdout, heap);

    printf("heap after first free\n");
    _free(data1);
    debug_heap(stdout, heap);

    printf("heap after second free\n");
    _free(data2);
    debug_heap(stdout, heap);


}

/**
 * Память закончилась, новый регион памяти расширяет старый.
 */
void memory_ended_case1_test(){
    printf("\nTest4. Memory ended case1.\n");

    void *heap = heap_init(10000);
    printf("initial heap\n");
    debug_heap(stdout, heap);

    _malloc(14000);
    printf("heap after allocation of big block\n");
    debug_heap(stdout, heap);
}

/**
 * Память закончилась, новый регион выделяется в другом месте
 */
void memory_ended_case2_test(){
    printf("\nTest5. Memory ended case2.\n");

    void *heap = heap_init(10000);
    printf("initial heap\n");
    debug_heap(stdout, heap);

    struct block_header* tmp = (struct block_header*) heap + 18000;

    if (mmap((void*) tmp, 200000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0) == 0)
        printf("mmap ended with error\n");

    printf("heap after mmap\n");
    _malloc(50000);
    debug_heap(stdout, heap);
}

