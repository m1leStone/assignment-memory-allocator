#include "tests.h"

int main(){
    common_malloc_test();

    free_one_block_test();

    free_two_blocks_test();

    memory_ended_case1_test();

    memory_ended_case2_test();
    return 0;
}
